![Concrete](https://static.concrete.com.br/uploads/2017/07/concrete.gif "Isso é um .gif")

---

__O Digital nos move.__
#### As pessoas nos movem.
### ~~Os desafios nos movem~~. 
## A _experiência_ nos move.
# **Nós movemos o mundo.**

---

> Somos parte da Accenture e estamos em constante movimento. Trabalhamos junto às maiores empresas do Brasil para garantir os melhores produtos digitais do mercado. Para isso, usamos design, desenvolvimento ágil e nossa cultura de inovação.

---

**Onboarding Tasks:**

1. Learn the basics about X 
2. Use Github 
3. Use Bitbucket 
4. Read articles, books, watch videos and talks about Scrum
5. Dockeeeeeeeeeer
6. Bubbles
7. This task

---

Trecho de código:

```javascript
for (var i=0; i>0; i++) {
    console.log("🖖");
}
```

---

| Site          | URL                                                    | Short description    |
| --------------|--------------------------------------------------------|--------------------- |
| Lorem         | lipsum.com                                             | Lorem ipsum sit amet |
| Procatinator  | [Do not click](http://procatinator.com "Procatinator") | The best site        |